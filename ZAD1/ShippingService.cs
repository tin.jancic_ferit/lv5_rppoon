﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class ShippingService
    {
        private double price_per_kg;
        public ShippingService(double price_per_kg)
        {
            this.price_per_kg = price_per_kg;
        }
        public double Delivery_Price(List<IShipable> items)
        {
            double delivery_price = 0;
            foreach(IShipable item in items)
            {
                delivery_price += price_per_kg * item.Weight; 
            }
            return delivery_price;
        }
    }
}
